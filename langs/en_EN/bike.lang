# Copyright (C) 2022 Mikael Carlavan <contact@mika-carl.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Générique
#

# Module label 'ModuleBikeName'
ModuleBikeName = Cycles
# Module description 'ModuleBikeDesc'
ModuleBikeDesc = Module managing cycles

#
# Page d'administration
#
BikeSetup = Module setup
Settings = Settings
BikeSetupPage = Setup module page
BikeOptions = Options
Description = Description
Value = Value
Action = Action
BikesNumberingModules = Bike numbering pattern
NextValue=Next value
NoDescription=No description
NoExample=No exemple
NotAvailable=Not available
Extrafields = Extrafields

#
# Page À propos
#
About = About
BikeAbout = About the module
BikeAboutPage = About page
BikeAboutDescLong = This module has been developped by <a href="http://www.mikael-carlavan.fr">Mikael Carlavan</a> for <a href="https://veloma.org">Véloma</a>.

# ChangeLog
ChangeLog = ChangeLog
ChangeLogVersion = Version
ChangeLogDate = Date
ChangeLogUpdates = Updates
BikeFirstVersion = First version

#
Bike = Cycle
Bikes = Cycless
BikesList = List
ListeBikes = Cycles list
BikesNew = New cycle
BikesArea = Cycles area
NoBike = No cycle
LastModifiedBikes = The %d last modified cycles
#
NewBike = New cycle
CreateBike = Create cycle
ShowBike = View the cycle
BikeCard = Cycle
Info = Info
Ref = Reference
BikeActive = Active
BikeName = Name
BikeNote = Note
BikeNoteType = Type
BikeTag = Tag
BikeStand = Stand
BikeUser = User
BikeAddDate = Add data
BikeCode = Cycle code
BikeNotePublic = Note (public)
BikeNotePrivate = Note (private)
DateCreation = Creation date
DateModificationShort = Modification date
UserAuthorId = User
ListOfBikes = Cycles list
CreatedByUsers = Users
NumOfBikes = Cycles number
BikePeriod = Period
BikeMonth = Month
BikeYear = Year
BikeAlreadyExists = The cycle already exists
#
BikeDeactivate = Deactivate
BikeActivate = Activate
MassActionDelete = Delete
DeleteBike = Delete
ValidateBike = Validate
SelectBike = Please select a cycle
ConfirmDeleteBike = Are you sure you want to delete this cycle?
ConfirmValidateBike = Are you sure you want to validate this cycle?
SearchIntoBikes = Cycles
SendBikeRef = Sending the cycle
ConfirmUnvalidateBike=Are you sure you want to set as draft the cycle <b>%s</b>
UnvalidateBike=Modify the cycle
ConfirmDeleteBikeLine = Are you sure you want to delete the cycle note?
DeleteBikeLine = Delete cycle note
#
BikeStatusValidated = Validated
BikeStatusValidatedShort = Validated
BikeStatusDraft = Draft
BikeStatusDraftShort = Draft
BikeStatus = Status
BikeActive = Active
BikeInactive = Inactive
#
BikeUserNotFound = The user account has not been found.
AccessNotAllowed = You're not allowed to do this action.
BikeNotFound = The cycle hasn't been found.
ErrorWhileRetrievingBikeList = Error while retrieving the cycles list.
ErrorWhileCreatingBike = Error while creating the cycle.
ErrorWhileUpdatingBike = Error while updating the cycle.
BikeHasBeenCreated = The cycle %s has been created.
BikeAlreadyExists = A cycle with this reference already exists.
#
BikeAddNewNote = Add a note


